const axios = require('axios');

class API {
    constructor({ url }) {
        this.url = url;
        this.endpoints = {}
        this.APP = axios.create({
            baseURL: 'https://test.mytablemesa.com/api/',
            timeout: 3000
        });
    }
}

export default API;