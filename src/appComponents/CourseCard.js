/* 
  Component that renders the course information cards
*/

import React from 'react';
// react-star-ratings is the component that renders the star rating
import StarRatings from 'react-star-ratings'; 
import Grid from '@material-ui/core/Grid';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Chip from '@material-ui/core/Chip';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import Box from '@material-ui/core/Box';

import './CourseCard.css';

const CourseCard = props => {
  const { image, title, institution, credits, price, rating } = props;
  return (
    <Card className="Card">
      <CardHeader
        className="CardHeader"
        avatar={
          <Avatar variant="square" aria-label="recipe" className="Avatar" src={image} />
        }
        action={
            <Chip className="Chip" label={`${credits} Credits`} />
        }
      />
      <CardContent>
        <Typography className="CourseTitle" variant="h5" color="textPrimary" component="p">
          <Box textAlign="left" m={1}>{title}</Box>
        </Typography>
        <Typography className="CourseInstitution" variant="h6" component="p">
          <Box textAlign="left" m={1}>{institution}</Box>
        </Typography>
      </CardContent>
      <CardActions disableSpacing>
          <Grid container>
            <Grid item xs={3}>
                <Typography className="CoursePrice" variant="h6" component="h2">
                    {price !== 0 ?`$ ${price}` : 'FREE'}
                </Typography>
            </Grid>
            <Grid item xs={9}>
                <StarRatings
                    rating={rating}
                    starRatedColor="#f6c943"
                    numberOfStars={5}
                    starDimension='16px'
                    starSpacing='1px'
                    name='rating'
                />
            </Grid>
          </Grid>
      </CardActions>
    </Card>
  );
}

export default CourseCard;