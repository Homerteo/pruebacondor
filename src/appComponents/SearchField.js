/*
  component that implements the search engine input
*/

import React from 'react';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import Input from '@material-ui/core/Input';
import InputAdornment from '@material-ui/core/InputAdornment';
import FormControl from '@material-ui/core/FormControl';
import SearchIcon from '@material-ui/icons/Search';

import './SearchField.css';

import { AppConsumer } from '../context/AppContext';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap'
  },
  margin: {
    margin: theme.spacing(1),
    padding: '10px 30px 10px 0px',
  },
  withoutLabel: {
    marginTop: theme.spacing(3),
  },
  textField: {
    width: 200,
  },
}));
  
const SearchField = props => {
  const classes = useStyles();
  const { appContext } = props;
  const { handleChange, filterValue } = appContext;

  return (
      <div className={classes.root}>
          <FormControl className={clsx(classes.margin, classes.textField)}>
          <Input
              className="SearchField"
              id="standard-adornment-password"
              type="search"
              name="filterValue"
              value={filterValue}
              onChange={e => handleChange(e)}
              placeholder="Search all courses"
              classes={{
                  underline: 'InputUnderline'
              }}
              startAdornment={
                <InputAdornment className="Adornment" variant="outlined" position="start">
                    <SearchIcon className="SearchIcon"/>
                </InputAdornment>
              }
          />
          </FormControl>
          </div>
    );
}

SearchField.propTypes = {
  handleChange: PropTypes.func.isRequired,
  filterValue: PropTypes.string.isRequired
}

export default props => (
  <AppConsumer>
    {({ handleChange, handleFilterCourses, filterValue }) => (
      <SearchField 
        {...props}
        appContext={{ handleChange, handleFilterCourses, filterValue }}
      />
    )}
  </AppConsumer>
)