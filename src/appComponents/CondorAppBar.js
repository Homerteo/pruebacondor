/* 
  Component that renders the application header bar
*/

import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Typography from '@material-ui/core/Typography';
import './CondorAppBar.css'

export default function CondorAppBar() {

  return (
    <div className='DivAppBar'>
      <AppBar position="static" className='AppBar'>
        <Typography variant="h6" className='Title'>
          <b>Condor Labs Test</b>
        </Typography>
      </AppBar>
    </div>
  );
}
