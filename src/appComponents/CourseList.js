/*
    component that implements the view of the course 
    list, the request of the data to the API and the 
    pagination using infinite scroll
*/

import React, { Component } from 'react';
import PropTypes from 'prop-types';
import GridList from '@material-ui/core/GridList';
import CourseCard from './CourseCard';
// react-infinite-scroller is the component that allows pagination using infinite scroll
import InfiniteScroll from 'react-infinite-scroller';
import axios from 'axios';

import { AppConsumer } from '../context/AppContext';

class CourseList extends Component {
    constructor(props) {
        super(props);

        this.state = {
            courseList: [],
            filterValue: '',
            hasMoreItems: true,
            offSet: 0,
            next: null
        };
    }

    componentDidUpdate(prevProps) {
        if (prevProps.appContext.filterValue !== this.props.appContext.filterValue) {

            this.setState({
                next: null,
            })
            // this.loadItems();
        }
    }
    

    loadItems = (page) => {
        const self = this;
        const { next } = this.state;
        let url = '';
        const baseUrl = 'http://localhost:1337/test.mytablemesa.com'
        if (next) {
            url = `${baseUrl}${next}`;
        } else {
            url = `${baseUrl}/api/courses?orderBy=popularity+desc&expand=provider&limit=24`;
        }

        axios.get(`${url}`)
            .then(data => {
                if (data) {
                    const { items, next } = data.data;
                    let { courseList } = self.state;
                    items.map(item => courseList.push(item));
                    if (next) {
                        self.setState({
                            courseList,
                            next
                        });
                    } else {
                        self.setState({
                            hasMoreItems: false
                        });
                    }
                }
            });
    };

    render() {
        const loader = <div className="loader">Loading ...</div>;
        let items= [];

        this.state.courseList.map(course => {
            items.push(
                <div className="row-xs-12 row-sm-6 row-md-3">
                    <CourseCard
                        image={`https://mytablemesa.com/${course.imageUrl}`}
                        title={course.name}
                        institution={course.provider.name}
                        credits={course.maximumCredits}
                        price={course.price}
                        rating={course.rating}
                    />
                </div>
            )
        });

        return (
            <InfiniteScroll
                pageStart={0}
                loadMore={this.loadItems.bind(this)}
                hasMore={this.state.hasMoreItems}
                loader={loader}
            >
                <GridList cols={3} cellHeight='280px' style={{overflow: 'hidden !important'}}>
                    {items}
                </GridList>
            </InfiniteScroll>
        );
    }
}

CourseList.propTypes = {
    appContext: PropTypes.object,
};

export default props => (
    <AppConsumer>
    {({ filterValue }) => (
      <CourseList 
        {...props}
        appContext={{ filterValue }}
      />
    )}
  </AppConsumer>
);