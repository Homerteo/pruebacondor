import React from 'react';
import CondorAppBar from './appComponents/CondorAppBar';
import SearchField from './appComponents/SearchField';
import CourseList from './appComponents/CourseList';

import { AppProvider } from './context/AppContext';
import './App.css';

const App = props => {
  return (
    <div className="App">
      <CondorAppBar />
      <div className="container">
        <div className="row">        
          <div className="col-sm-12"><SearchField /></div>
        </div>
        <div className="row"><CourseList /></div>
      </div> 
    </div>
  );
}

export default props => (
  <AppProvider>
    <App {...props} />
  </AppProvider>
);