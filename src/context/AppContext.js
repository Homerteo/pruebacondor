import React from 'react';
import PropTypes from 'prop-types';

const AppContext = React.createContext();

class AppProvider extends React.Component {
    constructor() {
        super();
        this.state = {
            filterValue: '',
        }
    };

    handleFilterCourses = () => {
        const { filterValue, courseList } = this.state;
        const searchExpression = filterValue.toLowerCase();
        const a = [];
        if (courseList.length >0) {
            for (let i = 0; i < courseList.length; i++) {
                let course = courseList[i];
                let name = course.name.toLowerCase();
    
                if (name.indexOf(searchExpression) >= 0) {
                    a.push(course);
                };
            };
            this.setState({
                filteredCourses: a
            });
        }
    }

    handleChange = event => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    render() {
        const { children } = this.props;
        return (
            <AppContext.Provider
                value={{
                    ...this.state,
                    handleChange: this.handleChange,
                    handleFilterCourses: this.handleFilterCourses,
                }}
            >
                {children}
            </AppContext.Provider>
        );
    }
}

AppProvider.propTypes = {
    children: PropTypes.element.isRequired
};

const AppConsumer = AppContext.Consumer;
export { AppProvider, AppConsumer };