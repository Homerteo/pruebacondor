This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).
## install and run corsproxy

## Before executing the project install and run corsproxy and run

Before running this project you must install corsproxy to avoid the CORS restriction that the API has to access local environments.
Insyall: `npm install -g corsproxy`
Run in a terminal: `corsproxy` 

## Available Scripts

## Runt the project

In the project directory, must use the command to run the project:

###  `yarn install`

Install all dependencies within the package package.json

### `yarn start`

Runs the app in the development mode.<br />
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br />
You will also see any lint errors in the console.
